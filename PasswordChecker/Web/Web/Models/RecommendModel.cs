﻿namespace Web.Models
{
    public class RecommendModel
    {
        public string Title { get; set; }
        public string Digit { get; set; }
        public string AllCase { get; set; }
        public string LowerCase { get; set; }
        public string UpperCase { get; set; }
        public string Symbol { get; set; }
        public string Length { get; set; }
    }
}
