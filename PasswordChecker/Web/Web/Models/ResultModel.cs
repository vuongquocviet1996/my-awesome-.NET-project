﻿namespace Web.Models
{
    public class ResultModel
    {
        public string Level;
        public string LevelColor;
        public string Message;
        public string Length;
        public string Complexity;
        public string Time;
        public string Error;

        public ResultModel()
        {
        }
    }
}
